# Jenkins

Sample jenkins pipeline created using DSL jobs and trigger build on push event using Generic webhook.

## Generic webhook

Webhook url to be added in the repository
```curl
<JENKINS URL>/generic-webhook-trigger/invoke?token=<TOKEN>
```
