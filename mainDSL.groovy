pipelineJob('Main') {
    configure {
        it / 'properties' / 'org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty' / 'triggers' << {
            'org.jenkinsci.plugins.gwt.GenericTrigger' {
                spec()
                'genericVariables' {
                    'org.jenkinsci.plugins.gwt.GenericVariable' {
                        expressionType('JSONPath')
                        key('ref')
                        value('$.ref')
                        regexpFilter()
                        defaultValue()
                    }
                }
                regexpFilterText('$ref')
                regexpFilterExpression('refs/heads/main')
                printPostContent('false')
                printContributedVariables('false')
                causeString('Generic Cause')
                token('myToken')
                silentResponse('false')
                overrideQuietPeriod('false')
            }
        }
    }

    logRotator {
        daysToKeep(15)
        numToKeep(15)
    }

    definition {
        cpsScm {
            scm {
                git {
                    branch('main')
                    remote {
                        url('https://gitlab.com/SanthoshNath/jenkins.git')
                    }
                }
            }
            scriptPath('Jenkinsfile.groovy')
            lightweight(false)
        }
    }
}
